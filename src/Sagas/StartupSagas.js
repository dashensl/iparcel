import firebase from 'react-native-firebase';


const startup = function* startup() {
    // check & add device token >> start
    const fDB = yield firebase.database();
    const fMSG = yield firebase.messaging();
    fMSG.getToken().then((currentToken) => {
        fDB.ref('/deviceTokens/').once('value').then((snapshot) => {
            const existingTokens = snapshot.val();
            if ((!existingTokens) || !existingTokens.includes(currentToken)) {
                const copy = existingTokens ? existingTokens.slice() : [];
                copy.push(currentToken);
                fDB.ref('deviceTokens/').set(copy);
            }
        });
    });
};

export default startup;
