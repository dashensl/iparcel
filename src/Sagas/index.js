import { takeLatest } from 'redux-saga/effects';
import { StartupTypes } from '../Redux/StartupRedux';
import { ParcelTypes } from '../Redux/ParcelRedux';
import { LocationTypes } from '../Redux/LocationRedux';
import startup from './StartupSagas';
import { startDriving } from './LocationSagas';
import { fetchParcels, startParcelTracking, startPickUpTracking, deliverParcel } from './ParcelSagas';
import { init } from '../API';

const API = init();
const rootSaga = function* rootSaga() {
    yield takeLatest(StartupTypes.STARTUP, startup);
    yield takeLatest([ParcelTypes.FETCH_PARCELS, ParcelTypes.DELIVER_PARCEL_SUCCESS], fetchParcels);
    yield takeLatest([ParcelTypes.START_PARCEL_TRACKING, ParcelTypes.FETCH_PARCELS_SUCCESS], startParcelTracking, API);
    yield takeLatest(LocationTypes.SET_RANDOM_NEARBY_LOCATION, startPickUpTracking, API);
    yield takeLatest(LocationTypes.START_DRIVING, startDriving, API);
    yield takeLatest(LocationTypes.STOP_DRIVING, deliverParcel, API);
};

export default rootSaga;
