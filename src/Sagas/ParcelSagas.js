import firebase from 'react-native-firebase';
import { Alert } from 'react-native';
import { delay } from 'redux-saga';
import { call, put, select, race } from 'redux-saga/effects';
import { ParcelActions } from '../Redux/ParcelRedux';
import { LocationActions } from '../Redux/LocationRedux';
import decode from '../Constants/Utils';
import { ListActions } from '../Redux/ListRedux';

const getParcelList = state => state.parcel.parcels;
const getListVisible = state => state.list.listVisible;
const getSelectedParcel = state => state.parcel.selectedParcel;
const getCurrentLocation = state => state.location.currentLocation;
const getStep2 = state => state.parcel.step2;

const fetchParcels = function* fetchParcels() {
    try {
        const snapshot = yield call(getParcels);
        if (snapshot.val()) {
            const res = [];
            const obj = snapshot.val();
            for (const key in obj) {
                res.push({ id: key, ...obj[key] });
            }
            yield put(ParcelActions.fetchParcelsSuccess(res));
        }
    } catch (e) {
        yield put(ParcelActions.fetchParcelsFailure('Unexpected Error'));
    }
};

const startParcelTracking = function* startParcelTracking(api, action) {
    const parcelList = yield select(getParcelList);
    const listVisible = yield select(getListVisible);
    if (parcelList.length === 0) {
        return;
    }
    const { pickup, dropoff } = parcelList[0];
    try {
        const response = yield call(api.getRoutes, { start: pickup, end: dropoff });
        const encodedPoints = response.data.routes[0].overview_polyline.points;
        const decodedPoints = decode(encodedPoints);
        yield put(LocationActions.setRandomNearbyLocation(pickup));
        if (listVisible) {
            yield put(ListActions.toggleListModal());
        }
        yield put(ParcelActions.startParcelTrackingSuccess(decodedPoints));
    } catch (e) {
        yield put(ParcelActions.startParcelTrackingFailure());
    }
};

const getParcels = () => new Promise(resolve => {
    const database = firebase.database();
    const connectionRef = database.ref('/parcels/');
    connectionRef.on('value', resolve);
});

const startPickUpTracking = function* startPickUpTracking(api) {
    try {
        const currentLocation = yield select(getCurrentLocation);
        const selectedParcel = yield select(getSelectedParcel);
        const { pickup } = selectedParcel;
        const response = yield call(api.getRoutes, { start: currentLocation, end: pickup });
        const encodedPoints = response.data.routes[0].overview_polyline.points;
        const decodedPoints = decode(encodedPoints);
        yield put(ParcelActions.startPickUpTrackingSuccess(decodedPoints));
    } catch (e) {
        yield put(ParcelActions.startPickUpTrackingFailure());
    }
};

const deliverParcel = function* deliverParcel() {
    yield delay(2 * 1000);
    yield put(LocationActions.endProcessing());
    const step2 = yield select(getStep2);
    if (step2.length > 0) {
        return;
    }
    try {
        const selectedParcel = yield select(getSelectedParcel);
        const res = updateParcel(selectedParcel);
        yield put(ParcelActions.deliverParcelSuccess());
        yield call(Alert.alert, 'Congratulations', 'You just delivered a parcel successfully!');
    } catch (e) {
        yield put(ParcelActions.deliverParcelFailure());
    }
};

const updateParcel = (parcel) => new Promise(() => {
    const database = firebase.database();
    database.ref(`/parcels/${parcel.id}`).set({ pickup: parcel.pickup, dropoff: parcel.dropoff, isPicked: true, isDelivered: true });
});


export {
    fetchParcels,
    startParcelTracking,
    startPickUpTracking,
    deliverParcel
};
