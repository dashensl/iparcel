import { call, put, select } from 'redux-saga/effects';
import { Alert } from 'react-native';
import { delay } from 'redux-saga';
import { ParcelActions } from '../Redux/ParcelRedux';
import { LocationActions } from '../Redux/LocationRedux';

const getStep1 = state => state.parcel.step1;
const getStep2 = state => state.parcel.step2;
// const getSelectedParcel = state => state.parcel.selectedParcel;
const getIsDriving = state => state.location.isDriving;

const startDriving = function* startDriving() {
    let isDriving = true;
    while (isDriving) {
        const step1 = yield select(getStep1);
        const step2 = yield select(getStep2);
        isDriving = yield select(getIsDriving);
        if (step1.length === 0 && step2.length === 0) {
            yield put(LocationActions.stopDriving());
            break;
        } else if (step1.length > 0) {
            yield put(ParcelActions.step1BurnDown());
            yield put(LocationActions.setLocation(step1[0]));
        } else {
            yield put(ParcelActions.step2BurnDown());
            yield put(LocationActions.setLocation(step2[0]));
        }
        yield delay(1 * 100);
    }
    // let step1 = yield select(getStep1);
    // let step2 = yield select(getStep2);
    // let isDriving = yield select(getIsDriving);
    // while (isDriving) {
    //     yield delay(Math.random() * 100);
    //     isDriving = yield select(getIsDriving);
    //     step1 = yield select(getStep1);
    //     step2 = yield select(getStep2);
    //     if (step1.length === 0 && step2.length === 0) {
    //         yield put(LocationActions.stopDriving());
    //         return yield put(ParcelActions.deliverParcel());
    //     }
    //     try {
    //         const target = step1.length > 0 ? step1 : step2;
    //         if (step1.length > 0) {
    //             if (step1.length === 1) {
    //                 yield call(Alert.alert, 'Pickup Success');
    //             }
    //             yield put(ParcelActions.step1BurnDown());
    //         } else {
    //             yield put(ParcelActions.step2BurnDown());
    //         }
    //         yield put(LocationActions.setLocation(target[0]));
    //     } catch (e) {
    //         yield put(LocationActions.stopDriving());
    //     }
    // }
};

export {
    startDriving
};
