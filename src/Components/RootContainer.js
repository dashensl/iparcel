import React, { Component } from 'react';
import { StatusBar, View, Text } from 'react-native';
import { connect } from 'react-redux';
import ReduxNavigation from '../Navigation/ReduxNavigation';
import Colors from '../Constants/Colors';

class RootContainer extends Component {
    componentDidMount() {

    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar barStyle='light-content' backgroundColor={Colors.darkgrey} />
                <ReduxNavigation />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
