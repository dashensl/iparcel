import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Colors from '../../Constants/Colors';

class ParcelCell extends Component {
    render() {
        const { data, selectedParcel, onPress } = this.props;
        const isSelected = selectedParcel && selectedParcel.id === data.id;
        return (
            <TouchableOpacity style={{ width: '100%', height: 40, alignItems: 'center', justifyContent: 'center' }} onPress={onPress}>
                <Text style={{ color: isSelected ? Colors.warning : 'white' }}>{isSelected ? 'Tracking' : ''} {data.id}</Text>
            </TouchableOpacity>
        );
    }
}
const mapStateToProps = state => {
    const { selectedParcel } = state.parcel;
    return {
        selectedParcel
    };
};
export default connect(mapStateToProps)(ParcelCell);
