import React, { Component } from 'react';
import { View, Text, TouchableOpacity, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import Colors from '../../Constants/Colors';
import { LocationActions } from '../../Redux/LocationRedux';
import { LAYOUT_SPRING_SCALE } from '../../Constants/index';

class DrivingButton extends Component {
    componentWillUpdate() {
        LayoutAnimation.configureNext(LAYOUT_SPRING_SCALE);
    }
    render() {
        const { isDriving, selectedParcel, processing } = this.props;
        const { startDriving, stopDriving } = this.props;
        const disabled = (selectedParcel === null || processing);
        return (
            <View style={[styles.buttonStyle, { backgroundColor: isDriving ? Colors.error : Colors.success, opacity: disabled ? 0.6 : 1 }]} >
                <TouchableOpacity disabled={disabled} onPress={isDriving ? stopDriving : startDriving} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: 'white', fontWeight: '500' }}>{isDriving ? 'Stop' : 'Start'}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = {
    buttonStyle: {
        position: 'absolute',
        width: 80,
        height: 80,
        backgroundColor: Colors.darkgrey,
        bottom: 5,
        zIndex: 100,
        alignSelf: 'center',
        borderRadius: 40
    }
};

const mapStateToProps = state => {
    const { isDriving, processing } = state.location;
    const { selectedParcel } = state.parcel;
    return {
        isDriving,
        selectedParcel,
        processing
    };
};
const mapDispatchToProps = dispatch => ({
    startDriving: () => dispatch(LocationActions.startDriving()),
    stopDriving: () => dispatch(LocationActions.stopDriving()),
});
export default connect(mapStateToProps, mapDispatchToProps)(DrivingButton);

