import React, { Component } from 'react';
import { View, FlatList, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import Colors from '../../Constants/Colors';
import ParcelCell from './ParcelCell';
import { ParcelActions } from '../../Redux/ParcelRedux';
import { LAYOUT_SPRING_SCALE } from '../../Constants';

class ParcelList extends Component {
    componentWillUpdate() {
        LayoutAnimation.configureNext(LAYOUT_SPRING_SCALE);
    }
    render() {
        const { container } = styles;
        const { parcels, startParcelTracking, listVisible } = this.props;
        return (
            <View style={[container, { width: listVisible ? '90%' : 0, height: listVisible ? '80%' : 0, top: listVisible ? 10 : 0 }]}>
                <FlatList
                    data={parcels}
                    keyExtractor={c => c.id}
                    renderItem={({ item }) =>
                        <ParcelCell data={item} onPress={() => startParcelTracking(item)} />
                    }
                />
            </View>
        );
    }
}

const styles = {
    container: {
        elevation: 2,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        alignSelf: 'center',
        position: 'absolute',
        backgroundColor: Colors.darkgrey,
        paddingHorizontal: 8,
        zIndex: 200,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    }
};
const mapStateToProps = state => {
    const { parcels } = state.parcel;
    const { listVisible } = state.list;
    return {
        parcels,
        listVisible
    };
};
const mapDispatchToProps = dispatch => ({
    startParcelTracking: (parcel) => dispatch(ParcelActions.startParcelTracking(parcel))
});
export default connect(mapStateToProps, mapDispatchToProps)(ParcelList);

