import React, { Component } from 'react';
import { View, TouchableOpacity, UIManager, LayoutAnimation, Text } from 'react-native';
import { connect } from 'react-redux';
import { Icon, Badge } from 'react-native-elements';
import firebase from 'react-native-firebase';
import MapView, { Polyline, Marker } from 'react-native-maps';
import StartupActions from '../../Redux/StartupRedux';
import { ParcelActions } from '../../Redux/ParcelRedux';
import Colors from '../../Constants/Colors';
import DrivingButton from './DrivingButton';
import ParcelList from './ParcelList';
import { LAYOUT_SPRING_SCALE } from '../../Constants';
import Images from '../../Constants/Images';

class MainScreen extends Component {
    static navigationOptions = ({ navigation, screenProps }) => {
        const { parcels } = screenProps.parcel;
        const { listVisible } = screenProps.list;
        const { processing } = screenProps.location;
        const { toggleListModal } = screenProps;
        return {
            headerTitle: 'iParcel',
            headerTitleStyle: { color: 'white', alignSelf: 'center', textAlign: 'center' },
            headerStyle: { backgroundColor: Colors.darkgrey },
            headerLeft: <View />,
            headerRight: (
                processing ?
                <View />
                :
                <TouchableOpacity onPress={toggleListModal} style={{ marginRight: 5, alignItems: 'center', justifyContent: 'center', padding: 10 }}>
                    <Icon name={listVisible ? 'md-close' : 'ios-notifications'} type='ionicon' color={'white'} />
                    {
                        (parcels !== null && parcels.length > 0 && !listVisible) &&
                        <Text style={{ color: 'white', position: 'absolute', top: 0, right: 0 }}>{String(parcels.length)} </Text>
                    }
                </TouchableOpacity>
            )
        };
    }

    componentDidMount() {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        this.props.startup();
        this.props.fetchParcels();
        firebase.messaging().onMessage(() => { this.props.fetchParcels(); });
    }
    componentWillUpdate() {
        LayoutAnimation.configureNext(LAYOUT_SPRING_SCALE);
    }

    render() {
        const { step1, step2, currentLocation, selectedParcel, processing } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <MapView
                    region={currentLocation ? {
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                        ...currentLocation
                    } : null}
                    style={{ width: '100%', height: '100%' }}
                >
                    {
                        step2 &&
                        <Polyline
                            coordinates={step2}
                            strokeColor={Colors.error} // fallback for when `strokeColors` is not supported by the map-provider
                            strokeWidth={5}
                        />
                    }
                    {
                        step1 &&
                        <Polyline
                            coordinates={step1}
                            strokeColor={Colors.darkSlateBlue} // fallback for when `strokeColors` is not supported by the map-provider
                            strokeWidth={5}
                        />
                    }
                    {currentLocation && <Marker coordinate={currentLocation} image={Images.car} />}
                    {selectedParcel && selectedParcel.pickup && <Marker coordinate={selectedParcel.pickup} image={Images.box} />}
                    {selectedParcel && selectedParcel.dropoff && <Marker coordinate={selectedParcel.dropoff} />}
                    {/* {<View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', backgroundColor: processing ? Colors.lightWhite : 'transparent' }}>
                        <Text style={{ color: processing ? Colors.darkgrey : 'transparent' }}>processing...</Text>
                    </View>} */}
                </MapView>
                <DrivingButton />
                <ParcelList />
            </View>
        );
    }
}
const mapStateToProps = state => {
    const { step1, step2, selectedParcel } = state.parcel;
    const { currentLocation, processing } = state.location;
    return {
        step1,
        step2,
        currentLocation,
        selectedParcel,
        processing
    };
};

const mapDispatchToProps = (dispatch) => ({
    startup: () => dispatch(StartupActions.startup()),
    fetchParcels: () => dispatch(ParcelActions.fetchParcels()),
});
export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
