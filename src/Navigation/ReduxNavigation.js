import React from 'react';
import { BackHandler, View } from 'react-native';
import { addNavigationHelpers, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import AppNavigation from './AppNavigation';
import { ParcelActions } from '../Redux/ParcelRedux';
import { ListActions } from '../Redux/ListRedux';

class ReduxNavigation extends React.Component {

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        const { dispatch, nav } = this.props;
        if (nav.index === 0) {
            return false;
        }
        dispatch(NavigationActions.back());
        return true;
    }

    render() {
        const { dispatch, nav } = this.props;
        const { parcel, toggleListModal, list, location } = this.props;
        const navigation = addNavigationHelpers({
            dispatch,
            state: nav
        });
        return (
            <AppNavigation navigation={navigation} screenProps={{ parcel, toggleListModal, list, location }} />
        );
    }
}
const mapStateToProps = state => ({ nav: state.nav, parcel: state.parcel, list: state.list, location: state.location });
const mapDispatchToProps = dispatch => ({
    toggleListModal: () => dispatch(ListActions.toggleListModal()),
});
export default connect(mapStateToProps, mapDispatchToProps)(ReduxNavigation);
