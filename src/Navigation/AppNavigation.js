import { StackNavigator } from 'react-navigation';
import MainScreen from '../Components/Main';


const PrimaryNav = StackNavigator({
    Main: {
        screen: MainScreen,
    },
}, {
        initialRouteName: 'Main',
        navigationOptions: {
            gesturesEnabled: false,
        },
    });

export default PrimaryNav;
