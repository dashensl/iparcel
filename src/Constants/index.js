import { Platform, LayoutAnimation } from 'react-native';

export const MAP_API_KEY = 'AIzaSyAXDbOO1hJWUKzq-5xW3jsFMYB_q8FNC6k';
export const LAYOUT_SPRING_SCALE = {
    duration: 300,
    create: {
        type: Platform.OS === 'ios' ? LayoutAnimation.Types.spring : LayoutAnimation.Types.easeIn,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 1,
    },
    update: {
        type: Platform.OS === 'ios' ? LayoutAnimation.Types.spring : LayoutAnimation.Types.easeIn,
        springDamping: 1,
    },
};
