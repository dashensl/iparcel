const colors = {
    primary: '#15496C',
    success: '#5cb85c',
    warning: '#fdc150',
    darkgrey: 'rgba(50,50,50,1)',
    lightWhite: 'rgba(255,255,255,0.7)',
    inactive: '#9B9B9B',
    error: '#EC6979',
    darkSlateBlue: 'rgb(37, 55, 120)',
    lightBlue: 'rgb(32, 121, 190)',
    lightGrey: 'rgb(200, 199, 204)'
};

export default colors;
