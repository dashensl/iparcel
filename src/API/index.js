import axios from 'axios';
import { MAP_API_KEY } from '../Constants';

export const init = () => {
    const instance = axios.create();
    const getRoutes = ({ start, end }) => instance.get(`https://maps.googleapis.com/maps/api/directions/json?origin=${start.latitude},${start.longitude}&destination=${end.latitude},${end.longitude}&key=${MAP_API_KEY}`);

    return {
        getRoutes
    };
};
