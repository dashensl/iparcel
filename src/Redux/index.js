import { combineReducers } from 'redux';
import configureStore from '../Config/CreateStore';
import rootSaga from '../Sagas';

export default () => {
    const rootReducer = combineReducers({
        nav: require('./NavigationRedux').reducer,
        parcel: require('./ParcelRedux').reducer,
        location: require('./LocationRedux').reducer,
        list: require('./ListRedux').reducer,
    });

    return configureStore(rootReducer, rootSaga);
};
