import { createActions, createReducer } from 'reduxsauce';

const { Types, Creators } = createActions({
    showParcel: null,
    hideParcel: null,
    showList: null,
    hideList: null,
    fetchParcels: null,
    fetchParcelsSuccess: ['parcels'],
    fetchParcelsFailure: null,
    startParcelTracking: ['parcel'],
    startParcelTrackingSuccess: ['step2'],
    startParcelTrackingFailure: ['error'],
    startPickUpTrackingSuccess: ['step1'],
    startPickUpTrackingFailure: ['error'],
    step1BurnDown: null,
    step2BurnDown: null,
    deliverParcel: null,
    deliverParcelSuccess: null,
    deliverParcelFailure: null
});

export const ParcelTypes = Types;
export const ParcelActions = Creators;

export const INITIAL_STATE = {
    visible: false,
    listVisible: false,
    parcels: [],
    history: [],
    loading: false,
    error: null,
    selectedParcel: null,
    step1: [],
    step2: []
};

export const showParcel = (state) => ({
    ...state,
    visible: true
});

export const hideParcel = (state) => ({
    ...state,
    visible: false
});

export const showList = (state) => ({
    ...state,
    listVisible: true
});

export const hideList = (state) => ({
    ...state,
    listVisible: false
});

export const fetchParcels = (state) => ({
    ...state,
    loading: true,
    error: false
});

export const fetchParcelsSuccess = (state, { parcels }) => {
    const pArr = [];
    const hArr = [];
    parcels.forEach(ele => {
        if (ele.isDelivered) {
            hArr.push(ele);
        } else {
            pArr.push(ele);
        }
    });
    return {
        ...state,
        selectedParcel: pArr.length > 0 ? pArr[0] : null,
        parcels: pArr,
        history: hArr,
        loading: false,
        error: false
    };
};

export const fetchParcelsFailure = (state, { error }) => ({
    ...state,
    loading: false,
    error
});

export const startParcelTracking = (state, { parcel }) => ({
    ...state,
    selectedParcel: parcel,
    loading: true
});
export const startParcelTrackingSuccess = (state, { step2 }) => ({
    ...state,
    loading: false,
    step2
});

export const startParcelTrackingFailure = (state, { error }) => ({
    ...state,
    step1: [],
    step2: [],
    loading: false,
    error
});

export const startPickUpTrackingSuccess = (state, { step1 }) => ({
    ...state,
    loading: false,
    step1
});

export const startPickUpTrackingFailure = (state, { error }) => ({
    ...state,
    step1: [],
    step2: [],
    loading: false,
    error
});
export const step1BurnDown = (state) => {
    const step1 = state.step1.slice();
    if (step1.length > 0) {
        step1.shift();
    }
    return {
        ...state,
        step1
    };
};
export const step2BurnDown = (state) => {
    const step2 = state.step2.slice();
    if (step2.length > 0) {
        step2.shift();
    }
    return {
        ...state,
        step2
    };
};

export const deliverParcel = (state) => ({
    ...state,
    loading: true,
});

export const deliverParcelSuccess = (state) => ({
    ...state,
    loading: false,
});
export const deliverParcelFailure = (state) => ({
    ...state,
    loading: false,
});

export const reducer = createReducer(INITIAL_STATE, {
    [Types.SHOW_PARCEL]: showParcel,
    [Types.HIDE_PARCEL]: hideParcel,
    [Types.SHOW_LIST]: showList,
    [Types.HIDE_LIST]: hideList,
    [Types.FETCH_PARCELS]: fetchParcels,
    [Types.FETCH_PARCELS_SUCCESS]: fetchParcelsSuccess,
    [Types.FETCH_PARCELS_FAILURE]: fetchParcelsFailure,
    [Types.START_PARCEL_TRACKING]: startParcelTracking,
    [Types.START_PARCEL_TRACKING_SUCCESS]: startParcelTrackingSuccess,
    [Types.START_PARCEL_TRACKING_FAILURE]: startParcelTrackingFailure,
    [Types.START_PICK_UP_TRACKING_SUCCESS]: startPickUpTrackingSuccess,
    [Types.START_PICK_UP_TRACKING_FAILURE]: startPickUpTrackingFailure,
    [Types.STEP1_BURN_DOWN]: step1BurnDown,
    [Types.STEP2_BURN_DOWN]: step2BurnDown,
    [Types.DELIVER_PARCEL]: deliverParcel,
    [Types.DELIVER_PARCEL_SUCCESS]: deliverParcelSuccess,
    [Types.DELIVER_PARCEL_FAILURE]: deliverParcelFailure,
});
