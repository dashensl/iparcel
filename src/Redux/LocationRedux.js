import { createActions, createReducer } from 'reduxsauce';

const { Types, Creators } = createActions({
    setRandomNearbyLocation: ['pickup'],
    startDriving: null,
    stopDriving: null,
    setLocation: ['location'],
    endProcessing: null,
});

export const LocationTypes = Types;
export const LocationActions = Creators;

export const INITIAL_STATE = {
    currentLocation: null,
    isDriving: null,
    processing: false
};

export const setRandomNearbyLocation = (state, { pickup }) => ({
    ...state,
    currentLocation: {
        latitude: (pickup.latitude * 1) + (((Math.random() * 2) - 1).toFixed(3) * 0.06),
        longitude: (pickup.longitude * 1) + (((Math.random() * 2) - 1).toFixed(3) * 0.06)
    }
});
export const startDriving = (state) => ({
    ...state,
    isDriving: true
});

export const stopDriving = (state) => ({
    ...state,
    isDriving: false,
    processing: true
});

export const endProcessing = (state) => ({
    ...state,
    processing: false
});
export const setLocation = (state, { location }) => ({
    ...state,
    currentLocation: location
});
export const reducer = createReducer(INITIAL_STATE, {
    [Types.SET_RANDOM_NEARBY_LOCATION]: setRandomNearbyLocation,
    [Types.START_DRIVING]: startDriving,
    [Types.STOP_DRIVING]: stopDriving,
    [Types.SET_LOCATION]: setLocation,
    [Types.END_PROCESSING]: endProcessing
});
