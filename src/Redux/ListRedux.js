import { createActions, createReducer } from 'reduxsauce';

const { Types, Creators } = createActions({
    toggleListModal: null,
});

export const ListTypes = Types;
export const ListActions = Creators;

export const INITIAL_STATE = {
    listVisible: false
};
export const toggleListModal = (state) => ({
    ...state,
    listVisible: !state.listVisible
});
export const reducer = createReducer(INITIAL_STATE, {
    [Types.TOGGLE_LIST_MODAL]: toggleListModal
});
