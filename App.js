import React, { Component } from 'react';
import { Provider } from 'react-redux';
import createStore from './src/Redux';
import './src/Config/ReactotronConfig';
import RootContainer from './src/Components/RootContainer';

const store = createStore();
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    );
  }
}

export default App;
